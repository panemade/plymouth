From eb40956898e35121525e253305f6d40b45cfbf23 Mon Sep 17 00:00:00 2001
From: Hans de Goede <hdegoede@redhat.com>
Date: Mon, 28 Feb 2022 16:36:58 +0100
Subject: [PATCH 6/6] ply-device-manager: verify_add_or_change(): Move
 local_console_is_text check

Move the local_console_is_text check outside of the
"if (subsytem == SUBSYSTEM_DRM)" block.

This check is equally relevant for SUBSYSTEM_FRAME_BUFFER.

Note by itself this is a no-op since verify_add_or_change() *always*
returns false for SUBSYSTEM_FRAME_BUFFER devices.

This is a preparation patch for making verify_add_or_change() not
return false when manager->device_timeout_elapsed is set.

Signed-off-by: Hans de Goede <hdegoede@redhat.com>
---
 src/libply-splash-core/ply-device-manager.c | 11 +++++------
 1 file changed, 5 insertions(+), 6 deletions(-)

diff --git a/src/libply-splash-core/ply-device-manager.c b/src/libply-splash-core/ply-device-manager.c
index bff4982..29b26fc 100644
--- a/src/libply-splash-core/ply-device-manager.c
+++ b/src/libply-splash-core/ply-device-manager.c
@@ -287,60 +287,64 @@ verify_drm_device (struct udev_device *device)
         if (ply_kernel_command_line_has_argument ("nomodeset"))
                 return true;
 
         /*
          * Some firmwares leave the panel black at boot. Allow enabling SimpleDRM
          * use from the cmdline to show something to the user ASAP.
          */
         if (ply_kernel_command_line_has_argument ("plymouth.use-simpledrm"))
                 return true;
 
         return false;
 }
 
 static bool
 create_devices_for_udev_device (ply_device_manager_t *manager,
                                 struct udev_device   *device)
 {
         const char *device_path;
         bool created = false;
 
         device_path = udev_device_get_devnode (device);
 
         if (device_path != NULL) {
                 const char *subsystem;
                 ply_renderer_type_t renderer_type = PLY_RENDERER_TYPE_NONE;
 
                 subsystem = udev_device_get_subsystem (device);
                 ply_trace ("device subsystem is %s", subsystem);
 
                 if (strcmp (subsystem, SUBSYSTEM_DRM) == 0) {
+                        if (!manager->device_timeout_elapsed && !verify_drm_device (device)) {
+                                ply_trace ("ignoring since we only handle SimpleDRM devices after timeout");
+                                return false;
+                        }
                         ply_trace ("found DRM device %s", device_path);
                         renderer_type = PLY_RENDERER_TYPE_DRM;
                 } else if (strcmp (subsystem, SUBSYSTEM_FRAME_BUFFER) == 0) {
                         ply_trace ("found frame buffer device %s", device_path);
                         if (!fb_device_has_drm_device (manager, device))
                                 renderer_type = PLY_RENDERER_TYPE_FRAME_BUFFER;
                         else
                                 ply_trace ("ignoring, since there's a DRM device associated with it");
                 }
 
                 if (renderer_type != PLY_RENDERER_TYPE_NONE) {
                         ply_terminal_t *terminal = NULL;
 
                         if (!manager->local_console_managed) {
                                 terminal = manager->local_console_terminal;
                         }
 
                         created = create_devices_for_terminal_and_renderer_type (manager,
                                                                                  device_path,
                                                                                  terminal,
                                                                                  renderer_type);
                         if (created) {
                                 if (renderer_type == PLY_RENDERER_TYPE_DRM)
                                         manager->found_drm_device = 1;
                                 if (renderer_type == PLY_RENDERER_TYPE_FRAME_BUFFER)
                                         manager->found_fb_device = 1;
                         }
                 }
         }
 
@@ -430,66 +434,61 @@ on_drm_udev_add_or_change (ply_device_manager_t *manager,
 
         /* Renderer exists, bail if this is not a change event */
         if (strcmp (action, "change"))
                 return;
 
         changed = ply_renderer_handle_change_event (renderer);
         if (changed) {
                 free_displays_for_renderer (manager, renderer);
                 create_pixel_displays_for_renderer (manager, renderer);
         }
 }
 
 static bool
 verify_add_or_change (ply_device_manager_t *manager,
                       const char           *action,
                       const char           *device_path,
                       struct udev_device   *device)
 {
         const char *subsystem = udev_device_get_subsystem (device);
 
         if (strcmp (action, "add") && strcmp (action, "change"))
                 return false;
 
         if (manager->local_console_managed && manager->local_console_is_text) {
                 ply_trace ("ignoring since we're already using text splash for local console");
                 return false;
         }
 
         subsystem = udev_device_get_subsystem (device);
 
-        if (strcmp (subsystem, SUBSYSTEM_DRM) == 0) {
-                if (!verify_drm_device (device)) {
-                        ply_trace ("ignoring since we only handle SimpleDRM devices after timeout");
-                        return false;
-                }
-        } else {
+        if (strcmp (subsystem, SUBSYSTEM_DRM)) {
                 ply_trace ("ignoring since we only handle subsystem %s devices after timeout", subsystem);
                 return false;
         }
 
         return true;
 }
 
 static bool
 duplicate_device_path (ply_list_t *events, const char *device_path)
 {
         struct udev_device *device;
         ply_list_node_t *node;
 
         for (node = ply_list_get_first_node (events);
              node; node = ply_list_get_next_node (events, node)) {
                 device = ply_list_node_get_data (node);
 
                 if (strcmp (udev_device_get_devnode (device), device_path) == 0)
                         return true;
         }
 
         return false;
 }
 
 static void
 process_udev_add_or_change_events (ply_device_manager_t *manager, ply_list_t *events)
 {
         const char *action, *device_path;
         struct udev_device *device;
         ply_list_node_t *node;
-- 
2.37.0.rc1

